/*Sirine Aoudj, 1935903*/
package LinearAlgebra;

public class Vector3d {

	final double x;
	final double y;
	final double z;
	
	public Vector3d(double x, double y, double z){
        this.x = x; 
        this.y = y;
        this.z = z;
}
	public double getx() {
		return this.x;
	}
	public double gety() {
		return this.y;
	}
	public double getz() {
		return this.z;
	}
	public double magnitude() {
		double magnitude = (Math.sqrt((Math.pow(this.x, 2))+(Math.pow(this.y,2))+(Math.pow(this.z, 2))));
		return magnitude;
	}
	public double dotProduct(Vector3d vector2) {
		double dotProduct = (this.x*vector2.getx())+(this.y*vector2.gety())+(this.z*vector2.getz());
		return dotProduct;
	}
	public Vector3d add(Vector3d vector2) {
		double x = this.x + vector2.getx();
		double y = this.y + vector2.gety();
		double z = this.z + vector2.getz();
		
		Vector3d newVector = new Vector3d(x,y,z);
		return newVector;
	}
		
	}
