/*Sirine Aoudj, 1935903*/
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class Vector3dTests {
	
	@Test
	void getVectorTest() {
		Vector3d vector = new Vector3d(1,2,3);
		assertEquals(1, vector.getx());
		assertEquals(2, vector.gety());
		assertEquals(3, vector.getz());
	}
	
	@Test
	void magnitudeTest() {
		Vector3d vector = new Vector3d(1,2,3);
		assertEquals(Math.sqrt(14),vector.magnitude());
	}
	
	@Test
	void dotProductTest() {
		Vector3d vector = new Vector3d(1,1,1);
		Vector3d vector2 = new Vector3d(2,2,2);
		assertEquals(6, vector.dotProduct(vector2));
	}
	
	@Test
	void addTest() {
		Vector3d vector = new Vector3d(1,1,1);
		Vector3d vector2 = new Vector3d(2,3,4);
		Vector3d vector3 = vector.add(vector2);
		assertEquals(3, vector3.getx());
		assertEquals(4, vector3.gety());
		assertEquals(5, vector3.getz());
	}

}
